from bs4 import BeautifulSoup
from traiDNDScrapper import HttpURLs


class Telemarketer(object):

    def __init__(self, district):

        self.registration = ""
        self.name = ""
        self.address = ""
        self.landline = ""
        self.mobile = ""
        self.email = ""
        self.date_of_registration = ""
        self.validity = ""
        self.district = district

    @staticmethod
    def all(district):

        url = HttpURLs.HttpURLs.telemarketers(district.state_id, district.district_id)
        content = HttpURLs.HttpURLs.content(url)

        # We can find what is required (TeleMarketers) in the Table
        soup = BeautifulSoup(content, 'html.parser')
        table = soup.find('form').find('table').find_all('tr')

        # First two entries are mostly useless, so we ignore them
        if len(table) <= 2:
            return []

        # Keep parsing rows
        marketers = []
        for rows in table[2:]:
            entry = Telemarketer.marketer_from_row(district, rows)
            if entry:
                marketers.append(entry)

        # And we are done
        return marketers

    # Parse a table row. if it is empty on serial number, then it is not a new row
    @staticmethod
    def marketer_from_row(district, row):

        cols = row.find_all('td')
        text = cols[0].text.isspace()
        if text:
            return None

        # Create the entry
        entry = Telemarketer(district)
        entry.registration = cols[1].text
        entry.name = cols[2].text
        entry.address = cols[3].text
        entry.landline = cols[4].text
        entry.mobile = cols[5].text
        entry.email = cols[6].text
        entry.date_of_registration = cols[7].text
        entry.validity = cols[8].text

        return entry
