from bs4 import BeautifulSoup
from traiDNDScrapper import HttpURLs


class District(object):
    def __init__(self, state_id, state_name):
        self.state_id = state_id
        self.state_name = state_name
        self.district_id = ''
        self.district_name = ''

    @staticmethod
    def districts(states):

        # Get URL and content
        url = HttpURLs.HttpURLs.districts(states.sid)
        content = HttpURLs.HttpURLs.content(url)

        # Result
        list = []

        # Parse
        soup = BeautifulSoup(content, 'lxml')
        dists = soup.find_all('district')
        for dist in dists:
            obj = District(states.sid, states.name)
            obj.district_id = dist.find('districtid').text
            obj.district_name = dist.find('districtname').text
            list.append(obj)

        # Done
        return list
