
import httplib2


class HttpURLs:

    @staticmethod
    def base():
        return "http://www.nccptrai.gov.in/nccpregistry"

    @staticmethod
    def states():
        root = HttpURLs.base()
        return root + "/listoftmstateanddistrict.misc?method=loadStates"

    @staticmethod
    def districts(state_id):
        root = HttpURLs.base()
        return root + '/listoftmstateanddistrict.misc?method=getDistrict&stateId=' + str(state_id)

    # List of Telemarketers in a district.
    @staticmethod
    def telemarketers(state, district):
        root = HttpURLs.base()
        suffix = '/ListofTelemarketers.misc?method=getTMRegList&districtcode='
        return root + suffix + str(district) + '&statecode=' + str(state)

    # Connect and get a response
    @staticmethod
    def content(url, method="GET"):
        connection = httplib2.Http()
        res, content = connection.request(url, method)
        return content
