
from bs4 import BeautifulSoup
from traiDNDScrapper import HttpURLs
import html

class States(object):

    def __init__(self, sid, name):
        self.name = name
        self.sid = sid

    # Find all the states in India through content parsing
    @staticmethod
    def all():

        # Get URL and content
        url = HttpURLs.HttpURLs.states()
        content = HttpURLs.HttpURLs.content(url)

        # Parse
        soup = BeautifulSoup(content, 'html.parser')
        row = soup.find('form').find('table').find_all('tr')[1]
        col = row.find_all('td')[1].find_all('option')
        options = col[2:]

        # Now extract the values
        states = []
        for option in options:
            name = html.unescape(option.text)
            id   = option.attrs['value']
            states.append(States(id, name))
        return states
