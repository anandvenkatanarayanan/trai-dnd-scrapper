
from traiDNDScrapper import States, District, Telemarketer

def main():

    # Empty marketers
    marketers = []

    # First get states, and districts within states and telemarketers within states.
    states = States.States.all()
    for state in states:
        districts = District.District.districts(state)
        for district in districts:
            entites = Telemarketer.Telemarketer.all(district)
            print(state.name, district.district_name, len(entites))
            marketers.extend(entites)

    # OK Done
    with open('marketers.csv', 'w') as myfile:
        for telemarketer in marketers:
            myfile.write(telemarketer.district.state_name + "," +
                                telemarketer.district.district_name + "," +
                                telemarketer.name + "," +
                                telemarketer.mobile + "," +
                                telemarketer.date_of_registration + "," +
                                telemarketer.validity  + "\n")


if __name__ == "__main__":
    main()
